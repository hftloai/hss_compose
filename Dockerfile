################## BEGIN INSTALLATION ######################
FROM ubuntu:16.04

# Install general dependencies
RUN apt update
RUN apt install -y \
	wget \
	apt-utils \
	sudo \
	psmisc

# Install dependecies
RUN apt install -y \
	autoconf \
	automake \
	bison \
	build-essential \
	cmake \
	cmake-curses-gui \
	doxygen \
	doxygen-gui \
	flex \
	gdb \
	pkg-config \
	git \
	subversion
# Install libraries
RUN apt install -y \
	libconfig8-dev \
	libgcrypt11-dev \
	libidn2-0-dev \
	libidn11-dev \
	libmysqlclient-dev \
	libpthread-stubs0-dev \
	libsctp1 \
	libsctp-dev \
	libssl-dev \
	libtool \
	openssl \
	python-pexpect \
	mysql-client

# remove_nettle_from_source
WORKDIR /tmp
RUN rm -rf nettle-2.5.tar.gz* nettle-2.5
RUN wget https://ftp.gnu.org/gnu/nettle/nettle-2.5.tar.gz
RUN tar -xzf nettle-2.5.tar.gz
WORKDIR /tmp/nettle-2.5/
RUN ./configure --disable-openssl --enable-shared --prefix=/usr
RUN make uninstall || true
WORKDIR /
# Install nettle library
RUN apt install -y \
	nettle-dev \
	nettle-bin

#Install FreeDiameter
RUN apt install -y \
	debhelper \
	g++ \
	gcc \
	libgcrypt-dev \
	libgnutls-dev \
	libpq-dev \
	libxml2-dev \
	mercurial \
	python-dev \
	ssl-cert \
      	swig
WORKDIR /tmp
RUN echo "Downloading 1.2.0 freeDiameter archive"
RUN rm -rf 1.2.0.tar.gz* freeDiameter-1.2.0 freediameter
RUN GIT_SSL_NO_VERIFY=true git clone https://gitlab.eurecom.fr/oai/freediameter.git -b eurecom-1.2.0
RUN mkdir freediameter/build
WORKDIR /tmp/freediameter/build
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local ../
RUN echo "Compiling freeDiameter"
RUN make -j`nproc`
RUN make install
RUN rm -rf /tmp/1.2.0.tar.gz /tmp/freeDiameter-1.2.0 /tmp/freediameter
WORKDIR /
################## END INSTALLATION ######################

################## INSTALL wait-for script ################
RUN git clone https://github.com/Eficode/wait-for.git
################## END INSTALL wait-for script ################

################## BEGIN CONFIGURING OAI ######################
# Create configuration folder
RUN rm -rf /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai
RUN mkdir /usr/local/etc/oai/freeDiameter

# Copy OpenAir Interface configuration files
COPY oai_conf/hss.conf /usr/local/etc/oai/
COPY oai_conf/freeDiameter /usr/local/etc/oai/freeDiameter

# Clone OpenAir Interface
RUN git clone -b develop https://gitlab.eurecom.fr/oai/openair-cn.git

# Build HSS
WORKDIR /openair-cn/scripts
RUN ./build_hss --clean

CMD ./run_hss
################## END CONFIGURING OAI ######################
